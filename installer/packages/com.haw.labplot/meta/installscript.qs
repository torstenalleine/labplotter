function Component()
{
    // default constructor
}

Component.prototype.createOperations = function()
{
    // call default implementation to actually install README.txt!
    component.createOperations();

    if (systemInfo.productType === "windows") {
        component.addOperation("CreateShortcut", "@TargetDir@/LabPlot.exe", "@AllUsersStartMenuProgramsPath@/LabPlot.lnk",
            "workingDirectory=@TargetDir@", "iconPath=@TargetDir@/LabPlot.exe",
            "iconId=0");
    }
}