import subprocess
import datetime

def gitstatus():
	p = subprocess.Popen([r'c:\Program Files\Git\bin\git.exe', 'status', '-uno', '--porcelain'], stdout=subprocess.PIPE)
	out, err = p.communicate()
	print out,err
	out = out.split('\n')

	total = 0
	modified = 0
	untracked = 0
	for line in out:
		s = line.split()
		if len(s) == 0:
			continue
		status = s[0].strip()
		total+=1
		if status == 'M':
			modified+=1
		if status == '??':
			untracked+=1
	return (total, modified, untracked)
def gitlastcommit():
	p = subprocess.Popen([r'c:\Program Files\Git\bin\git.exe', 'log', '--pretty=oneline'], stdout=subprocess.PIPE)
	out, err = p.communicate()
	out = out.split('\n')
	try:
		id = out[0].split()[0]
	except IndexError:
		id = "nogit"
	return id
def getdatestr():
	return datetime.datetime.now().strftime('%d.%m.%Y - %H:%M')

def make_header_content(major, minor, date, gitid):
	template = '''#ifndef VERSION_H
#define VERSION_H

namespace Version
{
 static const int MAJOR = %s;
 static const int MINOR = %s;
 static const char* DATESTR = "%s";
 static const char* GITID = "%s";
}

#endif // VERSION_H
	'''%(major, minor, date, gitid)
	return template

import sys
import os
try:
	progname, major, minor, outputfile = sys.argv
except ValueError:
	print "Usage: buildno <major> <minor> <outputfile>"
	exit(1)

os.chdir(os.path.abspath(os.path.dirname(outputfile)))
gitid = gitlastcommit()
(total, modified, untracked) = gitstatus()
if total > 0:
	gitid += ' (dirty)'
datestr = getdatestr()
content = make_header_content(major, minor, datestr, gitid)
print "buildno.py ==>", outputfile
with open(os.path.basename(outputfile), 'w') as fd:
	fd.write(content)
