#-------------------------------------------------
#
# Project created by QtCreator 2016-03-30T10:51:05
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LabPlot
TEMPLATE = app
CONFIG += c++11
CONFIG += qwt
win32:INCLUDEPATH += "C:/LabJack/Drivers"



SOURCES += main.cpp\
        labplotwindow.cpp \
    basedevice.cpp \
    labjack.cpp

HEADERS  += labplotwindow.h \
    basedevice.h \
    labjack.h \
    LJUD_DynamicLinking.h

FORMS    += labplotwindow.ui


MAJOR = 0
MINOR = 4
VERSION_HEADER = ../src/version.h

versiontarget.target = $$VERSION_HEADER
versiontarget.commands = python ../misc/buildno.py $$MAJOR $$MINOR $$VERSION_HEADER
versiontarget.depends = FORCE

PRE_TARGETDEPS += $$VERSION_HEADER
QMAKE_EXTRA_TARGETS += versiontarget



release: DESTDIR = $$OUT_PWD/../build/release
debug:   DESTDIR = $$OUT_PWD/../build/debug

OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.ui

RESOURCES += \
    res/schaltungsbilder.qrc

DISTFILES += \
    myapp.rc

RC_FILE = myapp.rc
